Case of 
	: (FORM Event:C1606.code=On Clicked:K2:4)
		
		  // Declare variables
		If (True:C214)
			
			C_OBJECT:C1216(\
				$oAPI)
			
			C_REAL:C285(\
				$rAmountInput;\
				$rAmountOutput)
			
			C_TEXT:C284(\
				$tCurrencyFrom;\
				$tCurrencyTo;\
				$tRefreshRate)
			
		End if 
		
		$tCurrencyFrom:=Form:C1466.FromCurrencyList
		$tCurrencyTo:=Form:C1466.ToCurrencyList
		
		$rAmountInput:=Form:C1466.AmountInput
		
		$tRefreshRate:=Form:C1466.settings.refresh_rate
		
		$oAPI:=ExchangeRate_Service_Get (Form:C1466.settings.api_file)
		
		
		Form:C1466.AmountOutput:=$rAmountInput*ExchangeRate_Get ($tCurrencyFrom;$tCurrencyTo;$oAPI;$tRefreshRate)
		
		
End case 