//%attributes = {}
  // Loads form variables to default or passed in values.
  // Meant to be used with the ExchangeRates form.
  //
  // $1 - object - The Form object variable.


C_OBJECT:C1216($1;$oForm)
$oForm:=$1

  // Declare variables
If (True:C214)
	
	C_COLLECTION:C1488(\
		$cSupportedCurrencies;\
		$cAPIFileNames)
	
	C_OBJECT:C1216(\
		$oSettings)
	
	C_POINTER:C301(\
		$pAPIList;\
		$pFromCurrencyList;\
		$pToCurrencyList)
	
End if 

  // Unpack settings.
If (True:C214)
	
	$cSupportedCurrencies:=$oForm.supported_currencies
	$cAPIFileNames:=$oForm.api_file_list
	
	$oSettings:=$oForm.settings
	
End if 


$pFromCurrencyList:=OBJECT Get pointer:C1124(Object named:K67:5;"FromCurrencyList")
$pToCurrencyList:=OBJECT Get pointer:C1124(Object named:K67:5;"ToCurrencyList")

COLLECTION TO ARRAY:C1562($cSupportedCurrencies;$pFromCurrencyList->)
COLLECTION TO ARRAY:C1562($cSupportedCurrencies;$pToCurrencyList->)

  // Set defaults to the first ones in the list.
$pFromCurrencyList->:=1
$oForm.FromCurrencyList:=$pFromCurrencyList->{1}

$pToCurrencyList->:=2
$oForm.ToCurrencyList:=$pFromCurrencyList->{2}

  // Set these defaults to zero.
$oForm.AmountInput:=0
$oForm.AmountOutput:=0

  // Settings
$pAPIList:=OBJECT Get pointer:C1124(Object named:K67:5;"APIList")
COLLECTION TO ARRAY:C1562($cAPIFileNames;$pAPIList->)

Popup_Set ("APIList";$oSettings.api_file)
Popup_Set ("CacheRefreshRateList";$oSettings.refresh_rate)