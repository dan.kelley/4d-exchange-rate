//%attributes = {}
  // Created by Dan Kelley, 2020
  // All methods are released under MIT license.

  // ExchangeRate is a program which calls external web services
  // to change the value of one currency into another value. It
  // features a caching system, a standardized API, and the ability
  // to use multiple services by loading different templates.

  // Note that this program is provided as-is as doesn't contain
  // all of the error handling it probably should.

  // Run this method to start the program.


  // Declare variables
If (True:C214)
	
	C_COLLECTION:C1488(\
		$cAPIFileList)
	
	C_OBJECT:C1216(\
		$fileSettings;\
		$fileSupportedCurrencies;\
		$folderAPI;\
		$oForm)
	
	C_LONGINT:C283(\
		$lWindow)
	
End if 

$fileSupportedCurrencies:=File:C1566("/RESOURCES/supported_currencies.json")
$fileSettings:=File:C1566("/RESOURCES/settings.json")

$oForm:=New object:C1471

  // Gets a list of all APIs.
$cAPIFileList:=Folder:C1567("/RESOURCES/API/")\
.files(Ignore invisible:K24:16)\
.extract("fullName")

Case of 
	: ($cAPIFileList.length=0)
		ALERT:C41("No API files found in "+$folderAPI.path)
		
	: ($fileSupportedCurrencies.exists=False:C215)
		ALERT:C41("supported_currencies.json could not be found")
		
	: ($fileSettings.exists=False:C215)
		ALERT:C41("settings.json could not be found")
		
	Else 
		
		$oForm.api_file_list:=$cAPIFileList
		$oForm.supported_currencies:=JSON Parse:C1218($fileSupportedCurrencies.getText();Is collection:K8:32)
		$oForm.settings:=JSON Parse:C1218($fileSettings.getText();Is object:K8:27)
		
		$lWindow:=Open form window:C675("ExchangeRates")
		DIALOG:C40("ExchangeRates";$oForm)
		CLOSE WINDOW:C154
		
		  // Save settings.
		$fileSettings.setText(JSON Stringify:C1217($oForm.settings;*))
		
End case 

