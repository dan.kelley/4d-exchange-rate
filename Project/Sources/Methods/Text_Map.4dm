//%attributes = {}
  // Maps an object to a text formatting string.
  //
  // Use: ("Hello, {name}!";New object("name";"MyName"))
  //      is the same as ("Hello, "+New object("name";"MyName").name+"!")
  //
  // $0 - text - A formatted string.
  //
  // $1 - text - A string to format.
  // $2 - object - An object to use keys for.

C_TEXT:C284($0)
C_TEXT:C284($1;$tTextToFormat)
C_OBJECT:C1216($2;$oMap)

$tTextToFormat:=$1
$oMap:=$2


  // Declare variables
If (True:C214)
	
	C_LONGINT:C283(\
		$i;\
		$lType)
	
	C_OBJECT:C1216(\
		$oFlattenedMap)
	
	C_TEXT:C284(\
		$key;\
		$nestedKey;\
		$noise;\
		$tDelimRight;\
		$tDelimLeft)
	
End if 


$oFlattenedMap:=New object:C1471

$noise:=String:C10(Milliseconds:C459+Random:C100)
$tDelimLeft:="$$$$fatC4t$$$$"+$noise+"$$$$f4tCat$$$$"
$tDelimRight:="$$$$f4tCat$$$$"+$noise+"$$$$fatC4t$$$$"

$tTextToFormat:=Replace string:C233($1;"{";$tDelimLeft)
$tTextToFormat:=Replace string:C233($tTextToFormat;"}";$tDelimRight)

For each ($key;$oMap)
	$lType:=OB Get type:C1230($oMap;$key)
	
	  // Flattens out object.
	Case of 
		: ($lType=Is object:K8:27)
			For each ($nestedKey;$oMap[$key])
				$oFlattenedMap[$key+"->"+$nestedKey]:=$oMap[$key][$nestedKey]
			End for each 
			
		: ($lType=Is collection:K8:32)
			For ($i;0;$oMap[$key].length)
				$oFlattenedMap[$key+"["+String:C10($i)+"]"]:=$oMap[$key][$i]
			End for 
			
		Else 
			$oFlattenedMap[$key]:=String:C10($oMap[$key])
			
	End case 
	
End for each 


For each ($key;$oFlattenedMap)
	$tTextToFormat:=Replace string:C233($tTextToFormat;$tDelimLeft+Replace string:C233($key;"->";".")+$tDelimRight;$oFlattenedMap[$key])
End for each 


$tTextToFormat:=Replace string:C233($tTextToFormat;$tDelimLeft;"{")
$0:=Replace string:C233($tTextToFormat;$tDelimRight;"}")

