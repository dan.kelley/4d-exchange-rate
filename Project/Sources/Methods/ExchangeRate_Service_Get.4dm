//%attributes = {}
  // Returns the exchange rate service between two currencies using the specified api.
  //
  // $0 - object - API documentation.
  // $1 - text - The file name of the API to get.

C_OBJECT:C1216($0;$oAPI)
C_TEXT:C284($1;$tFileName)

$tFileName:=$1

  // Declare variables
If (True:C214)
	
	C_OBJECT:C1216(\
		$file)
	
End if 

$file:=File:C1566("/RESOURCES/API/"+$tFileName)
If ($file.exists)
	$oAPI:=JSON Parse:C1218($file.getText();Is object:K8:27)
End if 


  // This returns null if there is no file.
$0:=$oAPI