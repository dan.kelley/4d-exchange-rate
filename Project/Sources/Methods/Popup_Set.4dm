//%attributes = {}
  // Sets a popup dropdown to the specific value, if it exists in the list.
  //
  // $1 - text - The object name of the popup dropdown.
  // $2 - variant - The value to find and set in the popup dropdown.

C_TEXT:C284($1;$tPopupObject)
C_VARIANT:C1683($2;$vValueToSet)

$tPopupObject:=$1
$vValueToSet:=$2

  // Declare variables
If (True:C214)
	
	C_LONGINT:C283(\
		$lFoundIndex)
	
	C_POINTER:C301(\
		$pPopup)
	
End if 


$pPopup:=OBJECT Get pointer:C1124(Object named:K67:5;$tPopupObject)
$lFoundIndex:=Find in array:C230($pPopup->;$vValueToSet)

If ($lFoundIndex#-1)
	
	$pPopup->:=$lFoundIndex
	
End if 