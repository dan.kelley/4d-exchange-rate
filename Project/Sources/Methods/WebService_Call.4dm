//%attributes = {}
  // Calls a web service.
  //
  // $0 - object - The response from the web service.
  // $1 - object - An api object to call.

C_OBJECT:C1216($1;$oAPI)
$oAPI:=$1

  // Declare variables
If (True:C214)
	
	C_LONGINT:C283(\
		$lError)
	
	C_OBJECT:C1216(\
		$eWebService_Calls;\
		$oResponse)
	
	C_TEXT:C284(\
		$tHTTPRequest;\
		$tTimestamp)
	
End if 


  // Format the url with its own object.
$tHTTPRequest:=Text_Map ($oAPI.format_url;$oAPI)
$lError:=HTTP Get:C1157($tHTTPRequest;$oResponse)

$tTimestamp:=Timestamp:C1445
$eWebService_Calls:=ds:C1482.WebService_Calls.new()
$eWebService_Calls.APIBase:=$oAPI
$eWebService_Calls.CreatedDate:=Date:C102($tTimestamp)
$eWebService_Calls.CreatedTime:=Time:C179($tTimestamp)
$eWebService_Calls.RequestKey:=$tHTTPRequest
$eWebService_Calls.Response:=$oResponse
$eWebService_Calls.save()


$0:=$oResponse