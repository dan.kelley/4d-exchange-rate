//%attributes = {}
  // Returns the value in a popup drop down.
  //
  // $0 - variant - The value currenly selected.
  // $1 - text - The name of the popup dropdown object.

C_VARIANT:C1683($0;$vSelectedValue)
C_TEXT:C284($1;$tPopupObject)

$tPopupObject:=$1

  // Declare variables
If (True:C214)
	
	C_POINTER:C301(\
		$pPopup)
	
End if 


$pPopup:=OBJECT Get pointer:C1124($tPopupObject)
$vSelectedValue:=$pPopup->{$pPopup->}


$0:=$vSelectedValue