//%attributes = {}
  // Returns the exchange rate for a specified API object.
  //
  // $1 - text - The currency to exchange from.
  // $2 - text - The currency to change to.
  // $3 - object - An API object.
  // $4 - text - How often to refresh the cache.

C_TEXT:C284($1;$tFromCurrency)
C_TEXT:C284($2;$tToCurrency)
C_OBJECT:C1216($3;$oAPI)
C_TEXT:C284($4;$tRefresh)

$tFromCurrency:=$1
$tToCurrency:=$2
$oAPI:=$3

If (Count parameters:C259>=4)
	$tRefresh:=$4
Else 
	$tRefresh:="never"
End if 

  // Declare variables
If (True:C214)
	
	C_BOOLEAN:C305(\
		$bUseCache)
	
	C_DATE:C307(\
		$dDate)
	
	C_OBJECT:C1216(\
		$eWebService_Calls;\
		$oResponse)
	
	C_TEXT:C284(\
		$tHTTPRequest)
	
	C_TIME:C306(\
		$hTime)
	
End if 


$oAPI.p.from_currency:=$tFromCurrency
$oAPI.p.to_currency:=$tToCurrency

$tHTTPRequest:=Text_Map ($oAPI.format_url;$oAPI)

$tTimestamp:=Timestamp:C1445
$hTime:=Time:C179($tTimestamp)
$dDate:=Date:C102($tTimestamp)

$bUseCache:=True:C214

  // Time/Date query adjustment.
Case of 
	: ($tRefresh="always")
		$bUseCache:=False:C215
		
	: ($tRefresh="every second")
		$hTime:=$hTime-?00:00:01?
		
	: ($tRefresh="every minute")
		$hTime:=$hTime-?00:01:00?
		
	: ($tRefresh="every hour")
		$hTime:=$hTime-?01:00:00?
		
	: ($tRefresh="daily")
		$dDate:=Add to date:C393($dDate;0;0;-1)
		
	: ($tRefresh="weekly")
		$dDate:=Add to date:C393($dDate;0;0;-7)
		
	: ($tRefresh="monthly")
		$dDate:=Add to date:C393($dDate;0;-1;0)
		
	: ($tRefresh="never")
		
		  // See if it's in the cache at all.
		$eWebService_Calls:=ds:C1482.WebService_Calls\
			.query("RequestKey = :1";$tHTTPRequest)\
			.orderBy("CreatedDate desc, CreatedTime desc")\
			.first()
		
		$bUseCache:=False:C215
		
End case 

If ($bUseCache)
	
	  // See if it's in the cache with our specified date range.
	$eWebService_Calls:=ds:C1482.WebService_Calls\
		.query("RequestKey = :1 AND CreatedDate >= :2 AND CreatedTime >= :3";$tHTTPRequest;$dDate;$hTime)\
		.orderBy("CreatedDate desc, CreatedTime desc")\
		.first()
	
End if 

  // Use cache if it's available.
If ($eWebService_Calls#Null:C1517)
	
	$oAPI:=OB Copy:C1225($eWebService_Calls.APIBase)
	$oResponse:=OB Copy:C1225($eWebService_Calls.Response)
	
	$oAPI.p.from_currency:=$tFromCurrency
	$oAPI.p.to_currency:=$tToCurrency
	
Else 
	
	$oResponse:=WebService_Call ($oAPI)
	
End if 


$0:=OB_GET ($oResponse;Text_Map ($oAPI.map.rate;$oAPI))