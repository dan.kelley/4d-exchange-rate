//%attributes = {}
  // Wrapper for OB_GET that gets nested objects.
C_VARIANT:C1683($0;$vReturnValue)

C_OBJECT:C1216($1;$object)
C_TEXT:C284($2;$key)

$object:=$1
$key:=$2


For each ($key;Split string:C1554($key;"."))
	If (OB Get type:C1230($object;$key)=Is object:K8:27)
		$object:=$object[$key]
	Else 
		$vReturnValue:=$object[$key]
	End if 
End for each 


$0:=$vReturnValue